//Product page
const mainImg = document.querySelector('.main .pd-cont .left-cont .main-img img');
const openedContainer = document.querySelector('.main .pd-cont .left-cont .open-cont');
const openedImg = document.querySelector('.main .pd-cont .left-cont .open-cont .open-img');
const openedCaption = document.querySelector('.main .pd-cont .left-cont .open-cont .caption');
const openFeature = document.querySelector('.fpd-cont');

//Open main img
function openImg() {
    openedContainer.style.display = 'flex';
    openedContainer.style.flexDirection = 'column';
    openedImg.src = mainImg.src;
    openedCaption.innerHTML = mainImg.alt;
}

//Close main img
function closeImg() {
    openedContainer.style.display = 'none';
}

//Close main img with 'Esc' key
document.addEventListener('keydown', function(e) {
    if (e.key === 'Escape') {
        closeImg();
    }
});

//Show sub img
function showSubImg(pic) {
    mainImg.src = pic;
}

openFeature.addEventListener('click', function() {
    window.location.href = 'product.html';
});