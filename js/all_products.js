document.addEventListener("DOMContentLoaded", function () {
    const TypeFilter = document.getElementById("typeFilter");
    const proContainer = document.getElementById("proContainer");
    const resultCount = document.getElementById("resultCount"); 
    const searchInput = document.getElementById("searchInput");
    const searchButton = document.getElementById("searchButton");

    const products = [
        { type: "Document", fee: "80", title: "Python", image: "../images/pro.jpg", link_pro: "../source/product.html"},
        { type: "Equipment", fee: "800", title: "Laptop", image: "../images/products/pro6.jpg", link_pro: "../source/product.html"},
        { type: "Document", fee: "80", title: "Artificial Intelligent for beginner", image: "../images/products/pro2.jpg", link_pro: "../source/product.html" },
        { type: "Document", fee: "80", title: "Python", image: "../images/products/pro3.jpg" , link_pro: "../source/product.html"},
        { type: "Equipment", fee: "80", title: "Python", image: "../images/products/pro8.jpg", link_pro: "../source/product.html"},
        { type: "Document", fee: "80", title: "Python", image: "../images/products/pro2.jpg", link_pro: "../source/product.html"},
        { type: "Document", fee: "80", title: "Python", image: "../images/pro.jpg", link_pro: "../source/product.html"},
        { type: "Equipment", fee: "180", title: "Mouse", image: "../images/products/pro7.jpg", link_pro: "../source/product.html"},
    ];

    function searchPro() {
        const searchInput = document.getElementById("searchInput");
        const searchTerm = searchInput.value.toLowerCase();

        const filteredPro = products.filter(product => {
            return product.title.toLowerCase().includes(searchTerm);
        });

        displayPro(filteredPro);
    }

    function filterPro() {
        const selectedType = TypeFilter.value;

        const filteredPro = products.filter(product => {
            return (
                (selectedType === "all" || product.type === selectedType)
            );
        });

        displayPro(filteredPro);
    }

    function displayPro(filteredPro) {
        proContainer.innerHTML = ""; 
        
        const newContainer = document.createElement("div");
        newContainer.classList.add("container"); 
        
        const proRow = document.createElement("div");
        proRow.classList.add("row");
        

        filteredPro.forEach(product => {
            const proBox = document.createElement("div");
            proBox.classList.add("col-12", "col-sm-6","col-md-4", "col-xl-3", "mt-4");


            proBox.innerHTML = `
            <div class="pro-box">
                <a href="${product.link_pro}">
                    <img src="${product.image}" class="card-img-top" alt="">   
                </a>
                <div class="desc">
                    <span>${product.type}</span>
                    <h5><a href="${product.link_pro}">${product.title}</a></h5>
                    <h4>$${product.fee}</h4>
                </div>
                <a href="#" class="add-cart">Add to Cart</a>
            </div>
            `;
            proRow.appendChild(proBox);
            newContainer.appendChild(proRow);
            proContainer.appendChild(newContainer);
        });
        resultCount.textContent = `${filteredPro.length}`;

    }
    TypeFilter.addEventListener("change", filterPro);
    searchButton.addEventListener("click", searchPro);

    // Lắng nghe sự kiện khi ô tìm kiếm rỗng thì tự động trả về tất cả khóa học
    searchInput.addEventListener("input", function (event) {
        if (event.inputType === "deleteContentBackward" && event.target.value === "") {
            displayPro(products);
        }
    });

    displayPro(products);

});