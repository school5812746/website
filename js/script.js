const nav = document.getElementById('navbar');
const bar = document.getElementById('bar');
const close = document.getElementById('close');
const topbutton = document.getElementById("topBtn");
const slides = document.getElementsByClassName("banner");
let slideIndex = 0;

// if bar is clicked, change navbar to class active (right is changed from -300px to 0px)
if (bar) 
{
    bar.addEventListener("click", () => {
        nav.classList.add('active');
    });
}

if (close) 
{
    close.addEventListener("click", () => {
        nav.classList.remove('active');
    });
}

showSlides();

function showSlides()
{
    let i;

    for (i = 0; i < slides.length; i++)
    {
        slides[i].style.display = "none";  
    }
    slideIndex++;

    if (slideIndex > slides.length) {slideIndex = 1}

    slides[slideIndex - 1].style.display = "block";  
    
    setTimeout(showSlides, 2000); // Change image every 2 seconds using recursion
}

// Back to top button
window.onscroll = function() {scrollFunc()};

// when the user scrolls down 20px from the top of the document, show the button
function scrollFunc() 
{
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) 
    {
        topbutton.style.display = "block";
    } 
    else 
    {
        topbutton.style.display = "none";
    }
}

// when the user clicks on the button, scroll to the top of the document
function goToTop() 
{
    document.body.scrollTop = 0; // For safari
    document.documentElement.scrollTop = 0; // For chrome, firefox, IE, etc
} 
