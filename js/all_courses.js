document.addEventListener("DOMContentLoaded", function () {
    const subjectFilter = document.getElementById("subjectFilter");
    const feeFilter = document.getElementById("feeFilter");
    const durationFilter = document.getElementById("durationFilter");
    const courseContainer = document.getElementById("courseContainer");
    const resultCount = document.getElementById("resultCount"); 
    const searchInput = document.getElementById("searchInput");
    const searchButton = document.getElementById("searchButton");

    const courses = [
        { subject: "Programming", fee: "280", duration: "12", title: "Python Data Structure", desc: "Python for beginner",image: "/website/images/courses/co3.jpg", link_course: "../source/course.html" },
        { subject: "AI", fee: "180", duration: "12", title: "Python",image: "/website/images/courses/co2.jpg", link_course: "../source/course.html" },
        { subject: "programming", fee: "80", duration: "8", title: "Web Programming with Python and JavaScript", image: "/website/images/courses/co1.jpg", link_course: "../source/course.html" },
        { subject: "AI", fee: "80", duration: "5", title: "Intro to Artificial Intelligence" ,image: "/website/images/courses/co4.jpg", link_course: "../source/course.html"},
        { subject: "Data Science", fee: "Free", duration: "8", title: "Data Science: R Basics" ,image: "/website/images/courses/co6.jpg", link_course: "../source/course.html"},
        { subject: "Programming", fee: "80", duration: "8", title: "Python Advanced" ,image: "/website/images/courses/co7.jpg", link_course: "../source/course.html"},
        { subject: "AI", fee: "80", duration: "12", title: "Elements of Artificial Intelligence" ,image: "/website/images/courses/co5.jpg", link_course: "../source/course.html"},
        { subject: "Data Science", fee: "Free", duration: "8", title: "Visualization" ,image: "/website/images/courses/co8.jpg", link_course: "../source/course.html"},
    ];

    const textEl = document.getElementById('text')
    const text = 'Work Hard, Get Reward!'
    let idx = 1
    let speed = 200 

    writeText()

    function writeText() {
        textEl.innerText = text.slice(0,idx)

        idx++

        if(idx > text.length) {
            idx = 1
        }

        setTimeout(writeText, speed)
    }

    function searchCourses() {
        const searchInput = document.getElementById("searchInput");
        const searchTerm = searchInput.value.toLowerCase();

        if (searchTerm === "") {
            displayCourses(courses);
            return;
        }

        const filteredCourses = courses.filter(course => {
            return course.title.toLowerCase().includes(searchTerm);
        });

        displayCourses(filteredCourses);
    }

    function filterCourses() {
        const selectedSubject = subjectFilter.value;
        const selectedFee = feeFilter.value;
        const selectedDuration = durationFilter.value;

        const filteredCourses = courses.filter(course => {
            return (
                (selectedSubject === "all" || course.subject === selectedSubject) &&
                (selectedFee === "all" || course.fee === selectedFee) &&
                (selectedDuration === "all" || course.duration === selectedDuration)
            );
        });

        displayCourses(filteredCourses);
    }

    function displayCourses(filteredCourses) {
        courseContainer.innerHTML = ""; 
        
        const newContainer = document.createElement("div");
        newContainer.classList.add("container"); 
        
        const coursesRow = document.createElement("div");
        coursesRow.classList.add("row");
        

        filteredCourses.forEach(course => {
            const courseBox = document.createElement("div");
            courseBox.classList.add("col-12", "col-sm-6", "col-xl-3", "mt-4");
            

            courseBox.innerHTML = `
            <div class="course-box">
                <a href="${course.link_course}">
                    <img src="${course.image}" class="card-img-top" alt="">    
                </a>
                <div class="course-type">
                    <i class="fa fa-code"></i>  
                    <p>${course.subject}</p>
                </div>
                <div class="desc">
                    <a href="${course.link_course}">
                        <h5>${course.title}</h5>
                    </a>
                    <p>${course.desc}</p> 
                    <div class="desc-box">
                        <div class="desc-left">
                            <p>Fee: <span>$${course.fee}</span></p>
                        </div>
                        <div class="desc-right">
                            <p>${course.duration} Weeks</p>
                        </div>
                    </div>
                </div>
            </div>
            `;
            coursesRow.appendChild(courseBox);
            newContainer.appendChild(coursesRow);
            courseContainer.appendChild(newContainer);
        });

        resultCount.textContent = `${filteredCourses.length}`;
    }

    subjectFilter.addEventListener("change", filterCourses);
    feeFilter.addEventListener("change", filterCourses);
    durationFilter.addEventListener("change", filterCourses);
    searchButton.addEventListener("click", searchCourses);

    // Lắng nghe sự kiện khi ô tìm kiếm rỗng thì tự động trả về tất cả khóa học
    searchInput.addEventListener("input", function (event) {
        if (event.inputType === "deleteContentBackward" && event.target.value === "") {
            displayCourses(courses);
        }
    });

    displayCourses(courses);

});
