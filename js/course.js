$(document).ready(function(){
    $('.featured-course').slick(
      {
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 1,
          draggable: false,
          dots: true,
          prevArrow:"<button type='button' class='slick-prev pull-left slick-arrow'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
          nextArrow:"<button type='button' class='slick-next pull-right slick-arrow'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
          responsive: [
            {
              breakpoint: 1025,
              settings: {
                slidesToShow: 3,
                infinite: true,
              }
            },
            {
              breakpoint: 821,
              settings: {
                slidesToShow: 2,
                infinite: true,
              }
            },
            {
              breakpoint: 500,
              settings: {
                slidesToShow: 1,
                arrows: false,
                infinite: false,
                draggable: true,
              }
            },
          ]
        }
    );
  });