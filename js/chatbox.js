const popup = document.querySelector('.chat-popup');
const chatBtn = document.querySelector('.chat-btn');
const submitBtn = document.querySelector('.submit');
const chatArea = document.querySelector('.chat-area');
const input = document.querySelector('.text-input');

// Chatbox button toggler
chatBtn.addEventListener('click', () => {
    popup.classList.toggle('active');
})

// Chatbox send message
submitBtn.addEventListener('click', () => {
    let userInput = input.value;
    let temp = `<div class="output-msg">
    <span class="user-msg">${userInput}</span>
    </div>`;

    // console.log(userInput);

    // insert html element into chatArea div as the last child of that div
    chatArea.insertAdjacentHTML("beforeend", temp);
    input.value = ''; // clear input area
})
