//Cart page
const productImg = document.querySelectorAll('.pd-img');
const openedContainer = document.querySelector('.open-img-cont');
const openedImg = document.querySelector('.open-img-cont .open-img');
const openedCaption = document.querySelector('.open-img-cont .caption');
const quantityInputs = document.querySelectorAll('.cart .table .row .cell:nth-child(4) input');
const checkout = document.querySelector('.checkout');
const pmMethod = document.querySelectorAll('.pm-method');
const pmDetails = document.querySelectorAll('.details');
const confirm = document.querySelector('.confirm');
const order = document.querySelector('.order');
const cardNameInput = document.getElementById('card-name');
const cardNumInput = document.getElementById('card-num');
const cardExpInput = document.getElementById('card-exp');
const cardCvvInput = document.getElementById('card-cvv');
const nameInput = document.getElementById('name');
const countryInput = document.getElementById('country');
const cityInput = document.getElementById('city');
const zipInput = document.getElementById('zip');
const addressInput = document.getElementById('address');
const phoneInput = document.getElementById('phone');
const emailInput = document.getElementById('email');

//Open product img
function openImg(index) {
    openedContainer.style.display = 'flex';
    openedContainer.style.flexDirection = 'column';
    openedImg.src = productImg[index].src;
    openedCaption.innerHTML = productImg[index].alt;
}

//Close product img
function closeImg() {
    openedContainer.style.display = 'none';
}

//Close product img with 'Esc' key
document.addEventListener('keydown', function(e) {
    if (e.key === 'Escape') {
        closeImg();
    }
});

//Increase quantity
quantityInputs.forEach(function(input) {
    input.addEventListener('change', function() {
        let newQuantity = input.value;
        let priceCell = input.parentElement.parentElement.querySelector('.cell:nth-child(5)');
        let originalPrice = parseFloat(priceCell.dataset.originalPrice);

        priceCell.textContent = '$' + (originalPrice * newQuantity).toFixed(2);

        updateCartTotal();
    });
});

//Update cart total
function updateCartTotal() {
    let rows = document.querySelectorAll('.cart .table .row:not(.head)');
    let subtotal = 0;

    rows.forEach(function(row) {
        let priceText = row.querySelector('.cell:nth-child(5)').textContent;
        let price = parseFloat(priceText.replace('$', ''));
        subtotal += price;
    });

    let couponText = document.querySelector('.coupon-total .total .table .row:nth-child(2) .cell:nth-child(2)').textContent;
    let coupon = parseFloat(couponText.replace('$', ''));
    let shippingText = document.querySelector('.coupon-total .total .table .row:nth-child(3) .cell:nth-child(2)').textContent;
    let shipping = parseFloat(shippingText.replace('$', ''));

    let total = subtotal + coupon + shipping;

    let subtotalCell = document.querySelector('.coupon-total .total .table .row:nth-child(1) .cell:nth-child(2)');
    subtotalCell.textContent = '$' + subtotal.toFixed(2);
    
    let couponCell = document.querySelector('.coupon-total .total .table .row:nth-child(2) .cell:nth-child(2)');
    couponCell.textContent = '$' + coupon.toFixed(2);
    
    let shippingCell = document.querySelector('.coupon-total .total .table .row:nth-child(3) .cell:nth-child(2)');
    shippingCell.textContent = '$' + shipping.toFixed(2);
    
    let finalTotalCell = document.querySelector('.coupon-total .total .table .row:nth-child(4) .cell:nth-child(2)');
    finalTotalCell.textContent = '$' + total.toFixed(2);
    
    let confirmTotalLink = document.querySelector('.confirm .total label a');
    confirmTotalLink.textContent = '$' + total.toFixed(2);
}

//Update cart total on page load
window.addEventListener('DOMContentLoaded', (event) => {
    let priceCells = document.querySelectorAll('.cart .table .row .cell:nth-child(5)');
    
    priceCells.forEach(function(cell) {
        let price = parseFloat(cell.textContent.replace('$', ''));
        cell.dataset.originalPrice = price;
    });
});

//Show checkout
function showCheckout() {
    checkout.style.display = 'flex';
}

//Select payment method and show details
function selectPaymentMethod(index) {
    for (let i = 0; i < pmMethod.length; i++) {
        pmMethod[i].style.backgroundColor = '#5C53C6';
        pmDetails[i].style.display = 'none';
    }
    
    if (pmMethod[index].style.backgroundColor === '#088178') {
        pmMethod[index].style.backgroundColor = '#5C53C6';
        pmDetails[index].style.display = 'none';
        confirm.style.display = 'none';
        order.style.display = 'none';
    } else {
        pmMethod[index].style.backgroundColor = '#088178';
        pmDetails[index].style.display = 'flex';
        confirm.style.display = 'flex';
        order.style.display = 'flex';
    }
}

// Limit the input length
function limitInputLength(inputField, maxLength) {
    inputField.addEventListener('input', function () {
        if (inputField.value.length > maxLength) {
            inputField.value = inputField.value.slice(0, maxLength);
        }
    });
}

// Specify the maximum length of each input field
document.addEventListener('DOMContentLoaded', function () {
    limitInputLength(cardNameInput, 100);
    limitInputLength(cardNumInput, 16);
    limitInputLength(cardExpInput, 4);
    limitInputLength(cardCvvInput, 3);
    limitInputLength(nameInput, 100);
    limitInputLength(countryInput, 100);
    limitInputLength(cityInput, 100);
    limitInputLength(zipInput, 6);
    limitInputLength(addressInput, 100);
    limitInputLength(phoneInput, 10);
    limitInputLength(emailInput, 100);
});

// Validate and place order
function validateAndPlaceOrder() {
    // Kiểm tra thông tin thanh toán Credit Card
    if (document.getElementById("pm1").checked) {
        let cardName = document.getElementById("card-name").value;
        let cardNumber = document.getElementById("card-num").value;
        let cardExp = document.getElementById("card-exp").value;
        let cardCVV = document.getElementById("card-cvv").value;

        if (!cardName || !cardNumber || !cardExp || !cardCVV) {
            alert("Please fill in all Credit Card details.");
            return;
        }
    }

    if (document.getElementById("pm2").checked) {
        let name = document.getElementById("name").value;
        let country = document.getElementById("country").value;
        let city = document.getElementById("city").value;
        let zip = document.getElementById("zip").value;
        let address = document.getElementById("address").value;
        let phone = document.getElementById("phone").value;
        let email = document.getElementById("email").value;

        if (!name || !country || !city || !zip || !address || !phone || !email) {
            alert("Please fill in all Cash on Delivery details.");
            return;
        }
    }

    if (!document.getElementById("order-cf").checked || !document.getElementById("terms-cf").checked) {
        alert("Please confirm the payment and agree to the terms and conditions.");
        return;
    }

    alert("Order placed successfully!");
}
